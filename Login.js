import { NavigationContainer } from '@react-navigation/native';
import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,
    
    } from 'react-native';

    import AsyncStorage from '@react-native-community/async-storage'
   // import Route from "./Route"

  // const userInfo={email:'abc@gmail.com',password:'pass123'}

     class Login extends Component {
        constructor(props){        
            super(props);        
            this.state={            
               email:'',
               password: ''        
            }   
        }
        _userLogin=() =>{
            var email = this.state.email;
            var password = this.state.password;
        
            
            if (email && password) { 

             
              fetch("https://cognicreate.com/MC-APP/api/login", {
                method: "POST",
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  email: email,
                  password: password,
                  login_type:'A'
                })
              })
              .then((response) => response.json())
              .then((responseData) => {
                console.log(responseData);
              // return responseData;
              //alert(responseData);
                alert(responseData);
               if (responseData.has_error === 0) {
                 console.log(responseData.user.name)  
              
                //alert('came here ')
                 AsyncStorage.setItem("loggedIn", "true");
                 AsyncStorage.setItem('lname', JSON.stringify(responseData.user.name));
                AsyncStorage.setItem('emailid', JSON.stringify(responseData.user.email));
                if(responseData.user.profile_image.length > 3)
                {
                  AsyncStorage.setItem('image', JSON.stringify((responseData.profile_image_path)+JSON.stringify(responseData.user.profile_image) ));

                }
             
             
               // AsyncStorage.setItem('last_name',responseData.user.last_name);
                //AsyncStorage.setItem('prof_pic',responseData.user.)
                alert('saved')
                this.setState({ loading: false });
                this.setState({email: ''})
                this.setState({password: ''})
                this.props.navigation.navigate("Dashboard");
              } 
             /*else {
              alert(response.data.message);
              this.setState({ loading: false });
            }*/

             })

             
               /*.then(response =>{ if (response.data.has_error === 0) {
              
                  //alert('came here ')
                  AsyncStorage.setItem("loggedIn", "true");
                  AsyncStorage.setItem('emailid', response.data.user.email);
                  
                  this.setState({ loading: false });
                  this.props.navigation.navigate("Dashboard");
                } 
               else {
                alert(response.data.message);
                this.setState({ loading: false });
              }*/
             .catch((error) => {
                console.log(error);
                alert(error);
            });


            
          }
        }
         data_Store = async () => {
            try {
                var email = this.state.email;
                var password = this.state.password;
            
                
                if (email && password) { 
    
                 
                 await fetch("https://cognicreate.com/MC-APP/api/login", {
                    method: "POST",
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      email: email,
                      password: password,
                      login_type:'A'
                    })
                  })
                  .then((response) => response.json())
                  .then((responseData) => {
                      console.log(responseData)
                    AsyncStorage.setItem('loginData', JSON.stringify(responseData));
                    alert('saved data')
                      
                  }
                  )}
            
              
            } catch (error) {
              console.error(error);
            }
          };

        

          Fetch_Data=async()=>{
             try{
            const storedValue = await AsyncStorage.getItem("loginData");
            const res= JSON.parse(storedValue).user.name;
            console.log(res)
             }
         catch (error) {
            Alert.alert('Error', 'There was an error.')
          }
        }

      

         
        
        
        render(){
            return(
                <View style={styles.container}>
                    <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
                
                    <Text style={styles.textStyle}>Login </Text>
                    <TextInput style={styles.inputBox}
                        onChangeText={(email) => this.setState({email})}
                         value={this.state.email}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    placeholder="Email"
                
                    placeholderTextColor = "#002f6c"
                    selectionColor="#fff"
                   // keyboardType="email-address"
                ></TextInput>
                    
                    <TextInput style={styles.inputBox}
                    onChangeText={(password) => this.setState({password})} 
                    value={this.state.password}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor = "#002f6c"
                    ref={(input) =>this.password = input}
                    />
                     <TouchableOpacity style={styles.button}> 
                    <Text style={styles.buttonText} onPress={this._userLogin}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}> 
                    <Text style={styles.buttonText}onPress={this.data_Store}>Store</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}> 
                    <Text style={styles.buttonText }onPress={this.Fetch_Data}>Show</Text>
                </TouchableOpacity>

                    <View style={styles.signupTextContent}>
                        <Text style={styles.signupText}>Don't have an account yet?</Text>
                         <TouchableOpacity>
                             <Text style={{color:'#002f6c'}} onPress={()=>this.props.navigation.navigate('Register')}>Signup</Text></TouchableOpacity>
                    </View>
     
    
                </View>
                
            );
         }

           /* _login=async()=>{

              
                if(userInfo.email===this.state.email && userInfo.password===this.state.password){
                    await AsyncStorage.setItem('isLoggedIn','1')
                    this.props.navigation.navigate('Register');
                }else{
                    alert('username or password is incorrect');
                }
            }*/
        }



    

    const styles = StyleSheet.create({
        container: {
            flex:1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor:'#c9c6c1'
        },
        textStyle:{
            fontSize:25,
            color: '#E37F32'


        },
        inputBox: {
            width: 300,
            backgroundColor: '#eeeeee', 
            
            borderRadius: 25,
            paddingVertical:10,
            paddingHorizontal: 16,
            fontSize: 16,
            color: '#002f6c',
            marginVertical: 10,
            alignItems:"center"
        },
        button: {
            width: 150,
            backgroundColor: '#F3DDB5',
            borderRadius: 15,
            marginVertical: 10,
            paddingVertical: 12
        },
        buttonText: {
            fontSize: 16,
            fontWeight: '500',
            color: '#4076c2',
            textAlign: 'center'
        },
        signupTextContent:{
         
            
            alignItems:'center',
            justifyContent:'flex-end',
            marginVertical:16,
            flexDirection:'row'
            
        },
        signupText:{
            color:'#E37F32',
            fontSize:16
        }
    });

    export default Login;