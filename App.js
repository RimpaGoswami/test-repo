
import 'react-native-gesture-handler';
import React,{Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import { createDrawerNavigator } from '@react-navigation/drawer';
import{createAppContainer,createSwitchNavigator} from 'react-navigation'
import { StyleSheet, 
  Text,
  View,
  TextInput,
  Button,
  StatusBar,
  TouchableOpacity,
  AsyncStorage
  } from 'react-native';
import Login from "./Login"
import Register  from "./Register"
import Dashboard from  "./Dashboard"
import Flat from "./Flat"
import Sidebar from "./Sidebar"


const Stack=createStackNavigator()
  



const Drawer=createDrawerNavigator();

/*const authStack=createStackNavigator({Login:Login})
const rootStack=createStackNavigator({
    Register:Register

});

class AuthLoading extends Component{
  constructor(props){
    super(props);
    this.loadData();

  }
  render(){
    return(
            <View>
              <Text>
                  Authentication!!!
              </Text>
            </View>

    );
  }
  loadData=async()=>{
    await AsyncStorage.getItem('isLoggedIn')
  }
}*/

 function DrawerRoute() {
  return (

    <Drawer.Navigator drawerContent={props=> <Sidebar {...props}/>}>
    
     
        <Drawer.Screen name="Dashboard" component={Dashboard}   />
       
      </Drawer.Navigator>
    
  );
}


export default function App()
  {
  return(


  <NavigationContainer>
    
    <Stack.Navigator>
      <Stack.Screen name="Login" component={Login}
          options={{ title: 'Welcome' }}
        />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Dashboard" component={DrawerRoute} options={{headerLeft:null}} />
        
    </Stack.Navigator>
  
  </NavigationContainer>
    
    
  
  
  
  

  )

}

