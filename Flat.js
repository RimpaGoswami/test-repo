import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,
    AsyncStorage,FlatList,Dimensions
    } from 'react-native';
import {Card} from 'react-native-cards';

/*class Flat extends Component{
    constructor(props){
        super(props);
        this.state={
            studentList:[
                {ID:'1',RollNo:'101',Name:'Robi Chandra'},
                {ID:'2',RollNo:'102',Name:'Bobi Dutta'},
                {ID:'3',RollNo:'103',Name:'Soma Das'},
                {ID:'4',RollNo:'104',Name:'Bhabesh Singh'}

            ]
        };
    }
     renderItem = ({ item }) => (
         
         <View style={styles.viewStyle}>
      <Text style={styles.textStyle}>  {item.Name} </Text>
     </View>
     );


    render(){
        return(
            
            
            <View>
                <FlatList>
                    data={this.state.studentList}
                    renderItem={this.renderItem}
                    keyExtractor={(item)=>item.ID}

            
            </FlatList>
            </View>
            
        );
    }
  
}
const styles = StyleSheet.create({container: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#c9c6c1'
},
 textStyle:{

    fontSize:15

},
viewStyle:{
    
    flexDirection:'row',
    marginLeft:20,
    padding:10

},


});*/


const Flat=()=>{

    const studentList=[
        {ID:'1',RollNo:'101',Name:'Robi Chandra'},
        {ID:'2',RollNo:'102',Name:'Bobi Dutta'},
        {ID:'3',RollNo:'103',Name:'Soma Das'},
        {ID:'4',RollNo:'104',Name:'Bhabesh Singh'}

    ];
     const renderItem = ({ item }) => (

     <Card style={styles.cardview}>
      <View>   
    <View style={{alignSelf:'center',flexDirection:'row'}} >
    <Text style={styles.textStyle}> RollNo: </Text>
     <Text style={styles.textStyle}>  {item.RollNo} </Text>
     
     </View>
     <View style={{alignSelf:'center',flexDirection:'row'}} >
    <Text style={styles.textStyle}> Name: </Text>
     <Text style={styles.textStyle}>  {item.Name} </Text>
     
     </View>
    </View>
    </Card>
    )
    const dimensions = Dimensions.get('window');
    const screenWidth = Dimensions.width;
      
    return(
    <View>
        <Text>I am Flat</Text>
        <TouchableOpacity> 
                    <Text>Show FlatList</Text>
                </TouchableOpacity>
                
    <FlatList style={{width: screenWidth}}
    data={studentList}
    renderItem={renderItem}
    keyExtractor={(item)=>item.ID}
    />


        
    </View>    
    )
}

const styles = StyleSheet.create({container: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#c9c6c1'
},
 textStyle:{
    flex:1,
    fontSize:15,

},
viewStyle:{
    
    flexDirection:'row',
    marginLeft:20,
    padding:10

},
cardview:{
    margin:5,
    borderRadius:10,
    backgroundColor:'pink'
}


});

export default Flat;

/*<FlatList>
    data={DATA}
    renderItem={renderItem}
    keyExtractor={(item)=>item.id}


</FlatList>*/


